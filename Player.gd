extends Node2D


# This callback gets triggered every 5 seconds by the Timer node in the scene.
func _on_Timer_timeout():
	print("Timer triggered callback.")
	
	# The randf() function returns a value between 0.0 and 1.0.
	# Here, I multiply this value by 100.0, to make it a random value between
	# 0.0 and 100.0. That way we can clearly see the sprite moving.
	var new_position = Vector2(randf() * 100.0, randf() * 100.0)
	
	# $Attack1 refers to a node named "Attack1" in the children of this node.
	# You can also use the get_node() method. But whichever one you use, it is
	# important to have the correct path to the node you want to reference.
	# For example:
	# get_node("/root/World/Player/Attack1")
	# or
	# $"/root/World/Player/Attack1"
	# or just
	# $Attack1
	# if the node is a direct child of this node.
	$Attack1.transform.origin = new_position

func _ready():
	# Make the Attack1 sprite visible, because we set it to hidden in the tree.
	get_node("/root/World/Player/Attack1").visible = true
